<?php

namespace Drupal\ogmedia_audience_lock;

use Drupal\Core\DependencyInjection\DependencySerializationTrait;
use Drupal\Core\Entity\EntityFormInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityReferenceSelection\SelectionPluginManagerInterface;
use Drupal\Core\Form\EnforcedResponseException;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\og\OgGroupAudienceHelperInterface;
use Drupal\ogmedia_group_stub\GroupStubService;
use Symfony\Component\HttpFoundation\RedirectResponse;

class AudienceLockHooks {

  use DependencySerializationTrait;
  use StringTranslationTrait;

  /**
   * The selection plugin manager interface.
   *
   * @var \Drupal\Core\Entity\EntityReferenceSelection\SelectionPluginManagerInterface
   */
  protected $selectionPluginManager;

  /**
   * The OG group audience helper.
   *
   * @var \Drupal\og\OgGroupAudienceHelperInterface
   */
  protected $groupAudienceHelper;

  /**
   * The group stub service, if exists.
   *
   * @var \Drupal\ogmedia_group_stub\GroupStubService|null
   */
  protected $groupStubService;

  /**
   * AudienceLockHooks constructor.
   *
   * @param \Drupal\Core\Entity\EntityReferenceSelection\SelectionPluginManagerInterface $selectionPluginManager
   * @param \Drupal\og\OgGroupAudienceHelperInterface $groupAudienceHelper
   * @param \Drupal\ogmedia_group_stub\GroupStubService|null $groupStubService
   */
  public function __construct(SelectionPluginManagerInterface $selectionPluginManager, OgGroupAudienceHelperInterface $groupAudienceHelper, GroupStubService $groupStubService = NULL) {
    $this->selectionPluginManager = $selectionPluginManager;
    $this->groupAudienceHelper = $groupAudienceHelper;
    $this->groupStubService = $groupStubService;
  }

  /**
   * Implements hook_entity_prepare_form().
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   * @param $operation
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   * @throws \Drupal\Core\TypedData\Exception\MissingDataException
   */
  public function hookEntityPrepareForm(EntityInterface $entity, string $operation, FormStateInterface $form_state) {
    if ($entity->isNew() && !$form_state->get('ogmedia_audience_lock_step2')) {
      $this->groupAudienceChoices($entity, TRUE);
    }
  }

  public function hookFormAlter(&$form, FormStateInterface $form_state, $form_id) {
    $formObject = $form_state->getFormObject();
    if ($formObject instanceof EntityFormInterface) {
      $entity = $formObject->getEntity();
      $groupAudienceFields = $this->groupAudienceHelper
        ->getAllGroupAudienceFields($entity->getEntityTypeId(), $entity->bundle());
      if ($groupAudienceFields) {
        /** @var \Drupal\Core\Entity\ContentEntityInterface $entity */
        $form['actions']['ogmedia_audience_lock_step1_next'] = [
          '#type' => 'submit',
          '#value' => $this->t('Next'),
          '#submit' => ['::submitForm', [$this, 'step1Next']],
          '#access' => FALSE,
        ];
        $step2 = !$entity->isNew() || $form_state->get('ogmedia_audience_lock_step2');
        if ($step2) {
          // Disable group audience fields.
          foreach ($groupAudienceFields as $groupAudienceFieldName => $groupAudienceField) {
            $this->disableRecursive($form[$groupAudienceFieldName]);
          }
        }
        else {
          // Skip step1 if no choice anyway.
          // In this case, entity values have been set in
          // ::hookEntityPrepareForm.
          if (
            !$this->groupAudienceChoices($entity)
            && ($redirectUrl = $this->checkRedirectToStubEdit($form_state))
          ) {
            // @see \Drupal\Core\Form\FormBuilder::retrieveForm
            $response = new RedirectResponse($redirectUrl->toString());
            throw new EnforcedResponseException($response);
          }
          // Step 1: Unset field access, except group audience fields..
          foreach (Element::children($form) as $key) {
            if (!isset($groupAudienceFields[$key]) && !in_array($key, [
              'actions',
              'form_id',
              'form_token',
              'form_build_id'
            ])) {
              $form[$key]['#access'] = FALSE;
            }
          }
          // Alter actions.
          foreach (Element::children($form['actions']) as $key) {
            $form['actions'][$key]['#access'] = FALSE;
          }
          $form['actions']['ogmedia_audience_lock_step1_next']['#access'] = TRUE;
        }
      }
    }
  }

  /**
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *
   * @throws \Drupal\Core\Entity\EntityMalformedException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function step1Next(array &$form, FormStateInterface $form_state) {
    if ($redirectUrl = $this->checkRedirectToStubEdit($form_state)) {
      $form_state->setRedirectUrl($redirectUrl);
    }
    else {
      // Fall back to form step 2.
      $form_state->setRebuild();
      $form_state->set('ogmedia_audience_lock_step2', TRUE);
    }
  }

  /**
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *
   * @return \Drupal\Core\Url|null
   * @throws \Drupal\Core\Entity\EntityMalformedException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  private function checkRedirectToStubEdit(FormStateInterface $form_state) {
    // @todo If someone needs this configurable, make it so.
    $shallTryToRedirectToStubEdit = $this->groupStubService;
    if ($shallTryToRedirectToStubEdit) {
      $formObject = $form_state->getFormObject();
      if ($formObject instanceof EntityFormInterface) {
        /** @var \Drupal\Core\Entity\ContentEntityInterface $entity */
        $entity = $formObject->getEntity();
        $redirectUrl = $this->groupStubService->makeStubEditUrl($entity);
        return $redirectUrl;
      }
    }
  }

  /**
   * Check which group audience fields still have data to enter.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   * @param bool $populate
   *   If true, populate the audience field if user has no choice.
   *
   * @return string[]
   *   The audience field definitions for which the user has a choice.
   */
  private function groupAudienceChoices(EntityInterface $entity, $populate = FALSE) {
    $fieldsWithChoice = [];
    foreach ($this->groupAudienceHelper->getAllGroupAudienceFields($entity->getEntityTypeId(), $entity->bundle()) as $fieldName => $fieldDefinition) {
      /** @var \Drupal\Core\Entity\ContentEntityInterface $entity */
      $selectionHandler = $this->selectionPluginManager->getSelectionHandler($fieldDefinition, $entity);
      $options = $this->ungroupOptions($selectionHandler->getReferenceableEntities());

      $noChoice = FALSE;
      if (!$fieldDefinition->isRequired() && count($options) === 0) {
        if ($populate) {
          $entity->set($fieldName, []);
        }
        $noChoice = TRUE;
      }
      if (
        $fieldDefinition->isRequired()
        && $fieldDefinition->getFieldStorageDefinition()->getCardinality() === 1
        && count($options) === 1
      ) {
        if ($populate) {
          $entity->set($fieldName, [key($options)]);
        }
        $noChoice = TRUE;
      }
      if (!$noChoice) {
        $fieldsWithChoice[$fieldName] = $fieldDefinition;
      }
    }
    return $fieldsWithChoice;
  }

  /**
   * @param $element
   */
  private function disableRecursive(&$element) {
    $element['#disabled'] = TRUE;
    foreach (Element::children($element) as $key) {
      $this->disableRecursive($element[$key]);
    }
  }

  /**
   * @param array $groups
   *
   * @return array
   */
  private function ungroupOptions(array $groups): array {
    $options = [];
    foreach ($groups as $group) {
      $options += $group;
    }
    return $options;
  }

}
