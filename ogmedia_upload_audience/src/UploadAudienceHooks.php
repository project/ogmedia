<?php

namespace Drupal\ogmedia_upload_audience;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element;
use Drupal\dropzonejs_eb_widget\Plugin\EntityBrowser\Widget\InlineEntityFormMediaWidget;
use Drupal\entity_browser\EntityBrowserFormInterface;
use Drupal\media\MediaInterface;
use Drupal\og\OgGroupAudienceHelperInterface;

class UploadAudienceHooks {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The OG group audience helper.
   *
   * @var \Drupal\og\OgGroupAudienceHelperInterface
   */
  protected $groupAudienceHelper;

  /**
   * UploadAudienceHooks constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   * @param \Drupal\og\OgGroupAudienceHelperInterface $groupAudienceHelper
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager, OgGroupAudienceHelperInterface $groupAudienceHelper) {
    $this->entityTypeManager = $entityTypeManager;
    $this->groupAudienceHelper = $groupAudienceHelper;
  }

  public function hookFormAlter(&$form, FormStateInterface $form_state, $form_id) {
    // @todo Do we really need this preparation step?
    foreach ($this->extractMediaFromFormState($form_state) as $media) {
      if (
        ($mediaAudienceFields = $this->groupAudienceHelper->getAllGroupAudienceFields($media->getEntityTypeId(), $media->bundle()))
        && count($mediaAudienceFields) === 1
        && ($groupEntityIds = $this->extractGroupEntityIds($form_state))
      ) {
        foreach ($mediaAudienceFields as $fieldName => $field) {
          $media->set($fieldName, $groupEntityIds);
          $form_state->set([
            'ogmedia_upload_audience__disable_field',
            $media->uuid(),
            $fieldName,
          ], $fieldName);
        }
      }
    }
  }

  public function hookInlineEntityFormEntityFormAlter(&$form, FormStateInterface $formState) {
    $entity = $form['#entity'] ?? NULL;
    if ($entity instanceof EntityInterface) {
      $disableFieldInfo = $formState->get('ogmedia_upload_audience__disable_field') ?? [];
      $uuid = $entity->uuid();
      $fieldNames = $disableFieldInfo[$uuid] ?? [];
      foreach ($fieldNames as $fieldName) {
        if (isset($form[$fieldName])) {
          $this->disableRecursive($form[$fieldName]);
        }
      }
    }
  }

  /**
   * Extract media entities.
   *
   * @param \Drupal\Core\Form\FormStateInterface $formState
   *
   * @return \Drupal\media\MediaInterface[]
   */
  private function extractMediaFromFormState(FormStateInterface $formState) {
    $formObject = $formState->getFormObject();
    if ($formObject instanceof EntityBrowserFormInterface) {
      $entityBrowser = $formObject->getEntityBrowser();
      $widgetId = $formState->get('entity_browser_current_widget');
      $widget = $entityBrowser->getWidget($widgetId);
      if ($widget instanceof InlineEntityFormMediaWidget) {
        if ($entities = $formState->get('uploaded_entities')) {
          return array_filter($entities, function ($entity) {
            return $entity instanceof MediaInterface;
          });
        }
      }
    }
    return [];
  }

  /**
   * Extract parent entity.
   *
   * Currently this simply extracts the first views filter.
   *
   * @todo Make this more generic.
   *
   * @return \Drupal\Core\Entity\EntityInterface|null
   */
  private function extractGroupEntityIds(FormStateInterface $formState) {
    $nodeIdList = $formState->get([
      'entity_browser',
      'widget_context',
      'views_arguments',
      0,
    ]);
    $nodeIds = $nodeIdList ? explode('+', $nodeIdList) : [];
    return $nodeIds;
  }

  private function disableRecursive(&$element) {
    if (is_array($element)) {
      $element['#disabled'] = TRUE;
      foreach (Element::children($element) as $key) {
        self::disableRecursive($element[$key]);
      }
    }
  }

}
