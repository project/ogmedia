<?php

namespace Drupal\ogmedia_group_stub;

use Drupal\Core\Controller\FormController;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Form\FormBuilderInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\og\Og;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Controller\ArgumentResolverInterface;

/**
 * Class GroupStubEntityFormControllerDecorator.
 *
 * @package Drupal\ogmedia_group_stub
 *
 * @internal
 */
class GroupStubEntityFormControllerDecorator extends GroupStubFormControllerDecoratorBase {

  /**
   * @var \Drupal\ogmedia_group_stub\GroupStubService
   */
  protected $groupStubService;

  /**
   * GroupStubFormControllerDecorator constructor.
   *
   * @param \Symfony\Component\HttpKernel\Controller\ArgumentResolverInterface $argument_resolver
   * @param \Drupal\Core\Form\FormBuilderInterface $form_builder
   * @param \Drupal\Core\Controller\FormController $decorated
   * @param \Drupal\ogmedia_group_stub\GroupStubService $groupStubService
   */
  public function __construct(ArgumentResolverInterface $argument_resolver, FormBuilderInterface $form_builder, FormController $decorated, GroupStubService $groupStubService) {
    parent::__construct($argument_resolver, $form_builder, $decorated);
    $this->groupStubService = $groupStubService;
  }

  /**
   * {@inheritDoc}
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   * @throws \Drupal\Core\Entity\EntityMalformedException
   * @throws \Drupal\Core\TypedData\Exception\MissingDataException
   *
   * @todo Consider to throw a EnforcedResponseException in hook_form_alter
   *   like \Drupal\Core\Form\FormBuilder::retrieveForm
   */
  public function getContentResult(Request $request, RouteMatchInterface $route_match) {
    // @see \Drupal\Core\Controller\FormController::getContentResult
    $formArg = $this->getFormArgument($route_match);
    /** @var \Drupal\Core\Entity\EntityFormInterface $formObject */
    $formObject = $this->getFormObject($route_match, $formArg);
    $entity = $formObject->getEntity();

    $entityTypeId = $entity->getEntityTypeId();
    $bundleId = $entity->bundle();
    if ($entity->isNew() && Og::isGroup($entityTypeId, $bundleId)) {
      /** @var \Drupal\Core\Entity\ContentEntityInterface $entity */
      $stubEditUrl = $this->groupStubService->makeStubEditUrl($entity);
      $redirectToStubEdit = RedirectResponse::create($stubEditUrl->toString());
      return $redirectToStubEdit;
    }
    else {
      return $this->decorated->getContentResult($request, $route_match);
    }
  }

}
