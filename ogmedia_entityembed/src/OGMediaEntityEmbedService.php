<?php

namespace Drupal\ogmedia_entityembed;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Routing\EnhancerInterface;
use Drupal\ogmedia_helpers\OGMediaHelpers;
use Symfony\Cmf\Component\Routing\RouteObjectInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class OGMediaEntityEmbedService.
 */
class OGMediaEntityEmbedService implements EnhancerInterface {

  /**
   * @var \Drupal\ogmedia_helpers\OGMediaHelpers
   */
  protected $helpers;

  /**
   * OGMediaEntityEmbedService constructor.
   *
   * @param \Drupal\ogmedia_helpers\OGMediaHelpers $helpers
   */
  public function __construct(OGMediaHelpers $helpers) {
    $this->helpers = $helpers;
  }

  /**
   * {@inheritDoc}
   */
  public function enhance(array $defaults, Request $request) {
    $isEntityEmbed = $defaults[RouteObjectInterface::ROUTE_NAME] === 'entity_embed.dialog';
    if ($isEntityEmbed) {
      $this->helpers->prepareGroupsInfoFromReferrer($request);
    }
    return $defaults;
  }

  function hookFormAlter(&$form, FormStateInterface $form_state, $form_id) {
    // @see \Drupal\entity_embed\Form\EntityEmbedDialog::getFormId
    // @see \Drupal\entity_embed\Form\EntityEmbedDialog::buildSelectStep
    if (
      $form_id === 'entity_embed_dialog'
      && isset($form['entity_browser'])
      && ($info = $this->helpers->getGroupsInfo())
    ) {
      $entityIds = reset($info);
      $entityIdList = implode('+', $entityIds);
      // @see \Drupal\entity_browser\Element\EntityBrowserElement::processEntityBrowser
      $form['entity_browser']['#widget_context']['views_arguments'] = [$entityIdList];
    }
  }

}
