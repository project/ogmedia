<?php

namespace Drupal\ogmedia_group_stub;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\content_moderation\ModerationInformationInterface;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Database\Query\AlterableInterface;
use Drupal\Core\Database\Query\SelectInterface;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityFormInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityReferenceSelection\SelectionPluginBase;
use Drupal\Core\Entity\EntityReferenceSelection\SelectionPluginManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\FieldableEntityInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\media\MediaInterface;
use Drupal\node\NodeInterface;
use Drupal\og\Og;
use Drupal\user\EntityOwnerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Class GroupStub.
 *
 * @package Drupal\ogmedia_group_stub
 *
 * @internal
 */
class GroupStubService {

  const STATE_STUB = 'ogmedia_group_stub';

  const MODERATION_STATE = 'moderation_state';

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The entity field manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * The selection plugin manager interface.
   *
   * @var \Drupal\Core\Entity\EntityReferenceSelection\SelectionPluginManagerInterface
   */
  protected $selectionPluginManager;

  /**
   * The OG group audience helper.
   *
   * @var \Drupal\og\OgGroupAudienceHelperInterface
   */
  protected $groupAudienceHelper;

  /**
   * The time service.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected $timeService;

  /**
   * The content moderation information service.
   *
   * @var \Drupal\content_moderation\ModerationInformationInterface|null
   */
  protected $moderationInformation;

  /**
   * Hooks constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entityFieldManager
   * @param \Drupal\Core\Entity\EntityReferenceSelection\SelectionPluginManagerInterface $selectionPluginManager
   * @param \Drupal\og\OgGroupAudienceHelperInterface $groupAudienceHelper
   * @param \Drupal\content_moderation\ModerationInformationInterface|null $moderationInformation
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager, EntityFieldManagerInterface $entityFieldManager, SelectionPluginManagerInterface $selectionPluginManager, \Drupal\og\OgGroupAudienceHelperInterface $groupAudienceHelper, TimeInterface $timeService, ModerationInformationInterface $moderationInformation = NULL) {
    $this->entityTypeManager = $entityTypeManager;
    $this->entityFieldManager = $entityFieldManager;
    $this->selectionPluginManager = $selectionPluginManager;
    $this->groupAudienceHelper = $groupAudienceHelper;
    $this->timeService = $timeService;
    $this->moderationInformation = $moderationInformation;
  }

  /**
   * Implements hook_entity_prepare_form().
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   * @param $operation
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *
   * @throws \Drupal\Core\TypedData\Exception\MissingDataException
   */
  public function hookEntityPrepareForm(EntityInterface $entity, $operation, FormStateInterface $form_state) {
    // If we're editing any stub, remove stub mark.
    if (!$entity->isNew() && $entity instanceof ContentEntityInterface) {
      $this->setWorkflowStateIfStubToInitial($entity);
    }
  }

  /**
   * Set workflow state to internal.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   */
  public function setWorkflowStateToStub(ContentEntityInterface $entity) {
    if ($this->moderationInformation) {
      if ($workflow = $this->moderationInformation->getWorkflowForEntity($entity)) {
        if ($workflow->getTypePlugin()->hasState(self::STATE_STUB)) {
          $entity->set(self::MODERATION_STATE, self::STATE_STUB);
        }
      }
    }
  }

  /**
   * Set workflow to initial state.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *
   * @throws \Drupal\Core\TypedData\Exception\MissingDataException
   */
  public function setWorkflowStateIfStubToInitial(ContentEntityInterface $entity) {
    if ($this->moderationInformation) {
      if ($workflow = $this->moderationInformation->getWorkflowForEntity($entity)) {
        $state = $entity->get(self::MODERATION_STATE)->getString();
        if ($state === self::STATE_STUB) {
          // ContentModeration changes the method signature :-/ .
          // @see \Drupal\content_moderation\Plugin\WorkflowType\ContentModeration::getInitialState
          /** @var \Drupal\content_moderation\Plugin\WorkflowType\ContentModeration $workflowType */
          $workflowType = $workflow->getTypePlugin();
          $initialState = $workflowType->getInitialState($entity)->id();
          $entity->set(self::MODERATION_STATE, $initialState);
        }
      }
    }
  }

  /**
   * Implements hook_cron().
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function hookCron() {
    $this->doGarbageCollection();
  }

  /**
   * Do garbage collection.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  private function doGarbageCollection() {
    if ($this->moderationInformation) {
      $someTimeAgoTimestamp = $this->timeService->getRequestTime() - 86400;

      $stubStateIds = \Drupal::entityQuery('content_moderation_state')
        ->condition(self::MODERATION_STATE, self::STATE_STUB)
        ->execute();
      /** @var \Drupal\content_moderation\Entity\ContentModerationStateInterface[] $stubStates */
      $stubStates = $this->entityTypeManager->getStorage('content_moderation_state')
        ->loadMultiple($stubStateIds);
      $entityIdsByType = [];
      foreach ($stubStates as $stubState) {
        $entityTypeId = $stubState->get('content_entity_type_id')->getString();
        $entityId = $stubState->get('content_entity_id')->getString();
        $entityIdsByType[$entityTypeId][$entityId] = $entityId;
      }

      foreach ($entityIdsByType as $entityTypeId => $entityIds) {
        $baseFieldDefinitions = $this->entityFieldManager->getBaseFieldDefinitions($entityTypeId);
        // We can only do GC if we have a 'changed' field.
        if (isset($baseFieldDefinitions['changed'])) {
          $idField = $this->entityTypeManager->getDefinition($entityTypeId)->getKey('id');
          $toDeleteIds = \Drupal::entityQuery($entityTypeId)
            ->condition($idField, array_values($entityIds), 'IN')
            ->condition('changed', $someTimeAgoTimestamp, '<')
            ->execute();
          $entityStorage = $this->entityTypeManager->getStorage($entityTypeId);
          $toDelete = $entityStorage
            ->loadMultiple($toDeleteIds);
          $entityStorage->delete($toDelete);
        }
      }
    }
  }

  /**
   * Implements hook_query_TAG_alter() for node_access.
   *
   * We want to...
   * - removes stubs from node listings
   * - remove stubs from all entityreference widgets
   * - NOT remove stubs BUT own ones from media audience selectors.
   *
   * For now we leave all stubs in media audience selectors, as they are auto-
   * selected anyway.
   *
   * @param \Drupal\Core\Database\Query\AlterableInterface $query
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function hookQueryNodeAccessAlter(AlterableInterface $query) {
    $doRemoveStubs = $query instanceof SelectInterface && !$this->isMediaAudienceSelector($query);
    if ($doRemoveStubs) {
      // @see \node_query_node_access_alter
      // @see \Drupal\node\NodeGrantDatabaseStorage::alterQuery
      $node = $this->extractBaseTableAlias($query);
      $condition = implode(' AND ', [
        "'node' = %alias.content_entity_type_id",
        "$node.vid = %alias.content_entity_revision_id",
        "$node.langcode = %alias.langcode",
      ]);
      $cm = $query->leftJoin('content_moderation_state_field_data', 'cm', $condition);
      $stubState = self::STATE_STUB;
      $query->where("$cm.moderation_state <> '$stubState' OR $cm.moderation_state IS NULL");
    }
  }

  /**
   * @param SelectInterface $query
   *
   * @return bool
   */
  private function isMediaAudienceSelector(SelectInterface $query) {
    $return = $query->hasTag('entity_reference')
      && ($selectionHandler = $query->getMetaData('entity_reference_selection_handler'))
      && $selectionHandler instanceof SelectionPluginBase
      && ($configuration = $selectionHandler->getConfiguration())
      && $configuration['entity'] instanceof MediaInterface;
    return $return;
  }

  /**
   * Extract base table alias.
   *
   * @param \Drupal\Core\Database\Query\AlterableInterface $query
   *
   * @return string
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Exception
   */
  private function extractBaseTableAlias(AlterableInterface $query) {
    // Copied from @see \node_query_node_access_alter.
    $tables = $query->getTables();
    $base_table = $query->getMetaData('base_table');
    // If the base table is not given, default to one of the node base tables.
    if (!$base_table) {
      /** @var \Drupal\Core\Entity\Sql\DefaultTableMapping $table_mapping */
      $table_mapping = \Drupal::entityTypeManager()->getStorage('node')->getTableMapping();
      $node_base_tables = $table_mapping->getTableNames();

      foreach ($tables as $table_info) {
        if (!($table_info instanceof SelectInterface)) {
          $table = $table_info['table'];
          // Ensure that 'node' and 'node_field_data' are always preferred over
          // 'node_revision' and 'node_field_revision'.
          if ($table == 'node' || $table == 'node_field_data') {
            $base_table = $table;
            break;
          }
          // If one of the node base tables are in the query, add it to the list
          // of possible base tables to join against.
          if (in_array($table, $node_base_tables)) {
            $base_table = $table;
          }
        }
      }

      // Bail out if the base table is missing.
      if (!$base_table) {
        throw new \Exception('Query tagged for node access but there is no node table, specify the base_table using meta data.');
      }
    }

    // Copied from @see \Drupal\node\NodeGrantDatabaseStorage::alterQuery .
    foreach ($tables as $table_alias => $tableinfo) {
      $table = $tableinfo['table'];
      if (!($table instanceof SelectInterface) && $table == $base_table) {
        return $table_alias;
      }
    }
    throw new \Exception("Query tagged for node access but can not find base table alias for table $base_table.");
  }

  /**
   * Implements hook_entity_access().
   *
   * Ensures that a user can view and edit their own stubs. If
   * session_node_access is installed, also for anon users.
   */
  public function hookEntityAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    $result = AccessResult::neutral();
    if (
      $entity instanceof FieldableEntityInterface
      && $entity->hasField(self::MODERATION_STATE)
      && $entity->get(self::MODERATION_STATE)->getString() === self::STATE_STUB
    ) {
      if (
        $entity instanceof EntityOwnerInterface
        && $account->id()
        && $entity->getOwnerId() === $account->id()
      ) {
        $result = AccessResult::allowed();
      }
      // @see \session_node_access_node_insert()
      elseif (
        $entity instanceof NodeInterface
        && ($nid = $entity->id())
        && in_array($nid, $_SESSION['session_node_access']['nodes_created'] ?? [])
      ) {
        $result = AccessResult::allowed();
      }
    }
    return $result;
  }

  /**
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *
   * @return \Drupal\Core\Url
   * @throws \Drupal\Core\Entity\EntityMalformedException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function makeStubEditUrl(ContentEntityInterface $entity) {
    // @todo Generalize this.
    if ($entity->getEntityTypeId() === 'node') {
      $entity->set('title', ' ');
    }
    $this->setWorkflowStateToStub($entity);
    $entity->save();
    $editUrl = $entity->toUrl('edit-form');
    return $editUrl;
  }

}
