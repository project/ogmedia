<?php

namespace Drupal\ogmedia_group_stub;

use Drupal\Core\Controller\FormController;
use Drupal\Core\Form\FormBuilderInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Controller\ArgumentResolverInterface;

/**
 * Class GroupStubFormControllerDecoratorBase.
 *
 * @package Drupal\ogmedia_group_stub
 *
 * @internal
 */
abstract class GroupStubFormControllerDecoratorBase extends FormController {

  /**
   * @var \Drupal\Core\Controller\FormController
   */
  protected $decorated;

  /**
   * GroupStubFormControllerDecoratorBase constructor.
   *
   * @param \Symfony\Component\HttpKernel\Controller\ArgumentResolverInterface $argument_resolver
   * @param \Drupal\Core\Form\FormBuilderInterface $form_builder
   * @param \Drupal\Core\Controller\FormController $decorated
   */
  public function __construct(ArgumentResolverInterface $argument_resolver, FormBuilderInterface $form_builder, FormController $decorated) {
    parent::__construct($argument_resolver, $form_builder);
    $this->decorated = $decorated;
  }

  /**
   * {@inheritDoc}
   */
  public function getContentResult(Request $request, RouteMatchInterface $route_match) {}

  /**
   * {@inheritDoc}
   */
  protected function getFormArgument(RouteMatchInterface $route_match) {
    return $this->decorated->getFormArgument($route_match);
  }

  /**
   * {@inheritDoc}
   */
  protected function getFormObject(RouteMatchInterface $route_match, $form_arg) {
    return $this->decorated->getFormObject($route_match, $form_arg);
  }

}
